drop database if exists DCD;
create database DCD;
use DCD;

create table if not exists Horaires(
	jour VARCHAR(9), 
	horaires_matin TIME(5) NOT NULL,
    horaires_aprem TIME(5) NOT NULL,
    ouverture TINYINT(1)
	) 
ENGINE=InnoDB DEFAULT CHARSET=UTF8;

create table if not exists Descriptions(
    TextResto BLOB TEXT() NOT NULL 
	) 
ENGINE=InnoDB DEFAULT CHARSET=UTF8;

create table if not exists Descriptions(
    TextResto BLOB TEXT() NOT NULL 
	) 
ENGINE=InnoDB DEFAULT CHARSET=UTF8;

create table if not exists Carte(
    disponibilité TINYINT(1) NOT NULL,
    menu VARCHAR(255) NOT NULL,
    prix_menu FLOAT NOT NULL, 
	) 
ENGINE=InnoDB DEFAULT CHARSET=UTF8;

create table if not exists Tarifs_Exemple(
    Exemple_prixmenu VARCHAR(255) NOT NULL 
	) 
ENGINE=InnoDB DEFAULT CHARSET=UTF8;

create table if not exists Avis(
    Text_avis VARCHAR(100) NOT NULL,
    Note_avis INT NOT NULL
	) 
ENGINE=InnoDB DEFAULT CHARSET=UTF8;


-- Commande de test --



